#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_CONVERTER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_CONVERTER_H

#include "bmp_reader.h"

const char *str_read_status(enum read_bmp_status status);

const char *str_write_status(enum write_bmp_status status);

#endif
