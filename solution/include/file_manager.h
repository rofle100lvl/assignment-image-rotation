#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H

#include <stdio.h>

enum file_open_status {
    FILE_OPEN_OK, FILE_OPEN_ERROR
};

enum file_open_status file_open_to_read(FILE **file_ptr, const char *filename);
enum file_open_status file_open_to_write(FILE **file_ptr, const char *filename);

void file_close(FILE* file);

#endif
