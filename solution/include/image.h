#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stddef.h>

struct image {
    size_t width;
    size_t height;
    struct pixel *data;
};

struct pixel {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};

struct image image_create(size_t width, size_t height);

struct image image_empty();

void image_destroy(struct image image);

struct pixel image_get_pixel(
        const struct image image,
        size_t x,
        size_t y
);

void image_set_pixel(
        struct image image,
        struct pixel pixel,
        size_t x,
        size_t y
);

#endif
