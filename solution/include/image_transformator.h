#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_TRANSFORMATOR_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_TRANSFORMATOR_H

#include "image.h"

struct image image_turn_over(struct image const image);

#endif
