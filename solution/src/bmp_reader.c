#include "bmp_reader.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>


static const uint32_t BF_TYPE = 0x4D42;
static const uint32_t BI_PLANES = 1;
static const uint32_t BI_BIT_COUNT = 24;
static const uint32_t BF_RESERVED = 0x0;
static const uint32_t BMP_HEADER_SIZE = 54;
static const uint32_t BI_SIZE = 40;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static size_t padding_calculate(struct image const* img) {
    return (4 - img->width * 3 % 4) % 4;
}

static size_t calculate_size(struct image const* image) {
    return image->height * (image->width * 3 + padding_calculate(image));
}

static enum read_bmp_status read_header(FILE *file, struct bmp_header *header) {
    const size_t items_count = 1;
    if (items_count == fread(header, sizeof(struct bmp_header), items_count, file)) {
        return READ_OK;
    }
    else return READ_HEADER_FREAD_ERROR;
}

static enum read_bmp_status find_validate_errors(const struct bmp_header *header) {
    if (header->bfType != BF_TYPE) return READ_HEADER_ERROR_HEADER_SIZE;
    if (header->bfReserved != BF_RESERVED) return READ_HEADER_ERROR_RESERVED;
    if (header->biPlanes != BI_PLANES) return READ_HEADER_ERROR_PLANES;
    if (header->biBitCount != BI_BIT_COUNT) return READ_HEADER_ERROR_BIT_PER_PIXEL;
    if (header->bOffBits != BMP_HEADER_SIZE) return READ_HEADER_ERROR_HEADER_SIZE;
    return READ_OK;
}


static enum read_bmp_status read_pixels(FILE *file_in, struct image *img) {
    const size_t padding = padding_calculate(img);
    const size_t size = img->width * sizeof(struct pixel);
    const size_t items_count = 1;

    for (size_t i = 0; i < img->height; i++) {
        void *ptr = img->data + (img->width * i);

        if (items_count != fread(ptr, size, items_count, file_in))
            return READ_PIXEL_FREAD_ERROR;
        if (fseek(file_in, padding, SEEK_CUR) != 0)
            return READ_PIXEL_FSEEK_ERROR;
    }
    return READ_OK;
}

static enum write_bmp_status write_header(FILE *file, struct image const *img) {
    const int32_t size = (int32_t) calculate_size(img);
    const size_t items_wait = 1;
    struct bmp_header header = {
            .bfType = BF_TYPE,
            .bfileSize = BMP_HEADER_SIZE + size,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = BI_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (items_wait == fwrite(&header, sizeof(struct bmp_header), items_wait, file))
        return WRITE_OK;
    else return WRITE_HEADER_FWRITE_ERROR;
}

static enum write_bmp_status write_pixels(FILE *file, struct image const *img) {
    const char padding_filler[] = {0, 0, 0, 0};
    const size_t padding = padding_calculate(img);

    for (size_t i = 0; i < img->height; i++) {
        const struct pixel *ptr = img->data + img->width * i;
        if (img->width != fwrite(ptr, sizeof(struct pixel), img->width, file))
            return WRITE_PIXEL_FWRITE_ERROR;
        if (padding != fwrite(padding_filler, sizeof(char), padding, file))
            return  WRITE_PIXEL_FWRITE_ERROR;
    }
    return WRITE_OK;
}

static bool is_header_read(enum read_bmp_status status) {
    return status != READ_OK;
}

static enum read_bmp_status read_header_from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    enum read_bmp_status status;

    status = read_header(in, &header);
    if (is_header_read(status)) return status;

    status = find_validate_errors(&header);
    if (is_header_read(status)) return status;
    *img = image_create(header.biWidth, header.biHeight);
    return status;
}

enum read_bmp_status read_from_bmp(FILE* in, struct image *img) {
    const enum read_bmp_status bmp_status = read_header_from_bmp(in, img);
    if (bmp_status == READ_OK) {
        const enum read_bmp_status bmp_status_with_pixels = read_pixels(in, img);
        return bmp_status_with_pixels;
    }
    return bmp_status;
}

enum write_bmp_status write_to_bmp(FILE* out, struct image *img) {
    const enum write_bmp_status bmp_status = write_header(out, img);
    if (bmp_status == WRITE_OK) {
        const enum write_bmp_status bmp_status_with_pixels = write_pixels(out, img);
        return bmp_status_with_pixels;
    }
    return bmp_status;
}
