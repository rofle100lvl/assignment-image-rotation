#include "bmp_reader.h"
#include "bmp_converter.h"

static const char *error_text_for_reading[] = {
        [READ_OK] = "Read OK!",
        [READ_HEADER_ERROR_SIGNATURE] = "Something wrong with signature",
        [READ_HEADER_ERROR_RESERVED] = "Something wrong with reserved",
        [READ_HEADER_ERROR_BIT_PER_PIXEL] = "Something wrong with count bit per pixel",
        [READ_HEADER_ERROR_PLANES] = "Something wrong with planes count",
        [READ_HEADER_ERROR_HEADER_SIZE] = "Something wrong with header size",
        [READ_HEADER_ERROR_SIZE] = "Something wrong with Bmp-file size",

        [READ_HEADER_FREAD_ERROR] = "Something wrong with reading files",
        [READ_HEADER_FSEEK_ERROR] = "Something wrong with fseek",

        [READ_HEADER_OTHER_ERROR] = "Some other problem",
        [READ_PIXEL_FSEEK_ERROR] = "Something wrong with pixels fseek",
        [READ_PIXEL_FREAD_ERROR] = "Something wrong with reading pixels",
        [READ_PIXEL_OTHER_ERROR] = "Some Other problem with pixels",
};

static const char *error_text_for_writing[] = {
        [WRITE_OK] = "Write Header Ok",
        [WRITE_HEADER_ERROR] = "Something wrong with File Writing",
        [WRITE_HEADER_OTHER_ERROR] = "Some other problem with write header",
        [WRITE_PIXEL_FWRITE_ERROR] = "Something wrong with pixels writing",
        [WRITE_PIXEL_OTHER_ERROR] = "Some other promlem with write pixels"
};

const char *str_write_status(enum write_bmp_status status) {
    return error_text_for_writing[status];
}

const char *str_read_status(enum read_bmp_status status) {
    return error_text_for_reading[status];
}





