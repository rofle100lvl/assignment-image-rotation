#include "bmp_reader.h"
#include "bmp_converter.h"
#include "file_manager.h"
#include "image_transformator.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

void print_string_err(const char *string) {
    fprintf(stderr, "%s\n", string);
}


int main(int argc, char **argv) {
    if (argc != 3) { print_string_err("Need 2 arguments to launch program"); }
    FILE *input_file = NULL;
    FILE *output_file = NULL;
    if (file_open_to_read(&input_file, argv[1]) != FILE_OPEN_OK) {
        print_string_err("Some problem with opening input file");
        return 1;
    }

    if (file_open_to_write(&output_file, argv[2]) != FILE_OPEN_OK) {
        print_string_err("Some problem with opening output file");
        file_close(input_file);
        return 1;
    }
    struct image source_image = image_empty();


    enum read_bmp_status bmp_read_status = read_from_bmp(input_file, &source_image);
    int status  = 0;

    if (bmp_read_status != READ_OK) {
        print_string_err(str_read_status(bmp_read_status));
        status = 1;
    }
    else {
        struct image rotated = image_turn_over(source_image);
        enum write_bmp_status bmp_write_status = write_to_bmp(output_file, &rotated);
        if (bmp_write_status != WRITE_OK) {
            print_string_err(str_write_status(bmp_write_status));
            status = 1;
        }
        image_destroy(rotated);
    }
    image_destroy(source_image);
    file_close(input_file);
    file_close(output_file);
    return status;
}
