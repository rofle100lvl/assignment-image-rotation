#include "file_manager.h"
#include <stdio.h>

enum file_open_status file_open_to_read(FILE **file_ptr, const char *filename) {
    *file_ptr = fopen(filename,"rb");
    if (*file_ptr == NULL) return FILE_OPEN_ERROR;
    else return FILE_OPEN_OK;
}

enum file_open_status file_open_to_write(FILE **file_ptr, const char *filename) {
    *file_ptr = fopen(filename,"wb");
    if (*file_ptr == NULL) return FILE_OPEN_ERROR;
    else return FILE_OPEN_OK;
}


void file_close(FILE * const file) {
    if (file != NULL) fclose(file);
}
