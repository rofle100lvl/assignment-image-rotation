#include "image.h"
#include <stdlib.h>

static size_t get_coordindate(struct image image, size_t x, size_t y) {
    return y * image.width + x;
}

struct image image_create(size_t width, size_t height) {
    if (width == 0 || height == 0)
        return (struct image) {.width = 0, .height = 0, .data = NULL};
    return (struct image) { .width = width, .height = height,
            .data = malloc(width * height * sizeof(struct pixel)) };
}

struct image image_empty() {
    return image_create(0, 0);
}

void image_destroy(struct image image) {
    free(image.data);
}

struct pixel image_get_pixel(const struct image image, size_t x, size_t y) {
    return image.data[get_coordindate(image, x, y)];
}

void image_set_pixel(struct image image, struct pixel pixel, size_t x, size_t y) {
    image.data[get_coordindate(image, x, y)] = pixel;
}
