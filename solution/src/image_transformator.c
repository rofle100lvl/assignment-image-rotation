#include "image_transformator.h"

struct image image_turn_over(struct image const image) {
    struct image new_image = image_create(image.height, image.width);
    for (size_t i = 0; i < image.width; i++) {
        for (size_t j = 0; j < image.height; j++) {
            const struct pixel pixel = image_get_pixel(image, i, j);
            image_set_pixel(new_image, pixel, new_image.width - j - 1, i);
        }
    }
    return new_image;
}
